const { default: axios } = require("axios");
const { CieloService, AxiosService, MomentService, DotenvService } = require("../../../src/services/index.js");

describe("CieloService - Testes de Integração", () => {
    let dotenvService = {};
    let requestService = {};
    let timeService = {};
    let pagamentoService = {};
    
    beforeAll(() => {
        dotenvService = new DotenvService();

        requestService = new AxiosService(axios, { 
            baseUrl: dotenvService.get("CIELO_HOST"), 
            options: {
                headers: {
                    "Content-Type": "application/json",
                    MerchantId: dotenvService.get("CIELO_MERCHANT_ID"),
                    MerchantKey: dotenvService.get("CIELO_MERCHANT_KEY")
                }
            }
        });

        timeService = new MomentService();
        pagamentoService = new CieloService({ requestService, timeService });
    });

    /**
     * Cartões terminados em 0, 2 ou 4 representam cartões válidos.
     */
    test("Deve criar um transação.", async () => {
        

        const cartoes = [
            {
                numero: "2311.2342.5235.4130",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100
            },
            {
                numero: "2311.2342.5235.4131",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            },
            {
                numero: "2311.2342.5235.4134",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            }
        ];

        const results = [];

        for (const cartao of cartoes) {
            results.push(pagamentoService.criarTransacao(cartao));
        }

        const pagamentos = await Promise.all(results);

        for (const pagamento of pagamentos) {
            expect(pagamento.success).toBeTruthy();
        }

    }, 30000);

    /**
     * Cartões terminados em 2, 3, 5, 6, 7, 8 representão cartões inválidos.
     * 
     * O erro associado a cada um é direrente, mas isso não faz diferença para o escopo do projeto.
     */
    test("Deve falhar ao criar uma transação se o cartão for inválido.", async () => {
        const cartoes = [
            {
                numero: "2311.2342.5235.4132",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            },
            {
                numero: "2311.2342.5235.4133",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            },
            {
                numero: "2311.2342.5235.4135",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            },
            {
                numero: "2311.2342.5235.4136",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            },
            {
                numero: "2311.2342.5235.4137",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100

            },
            {
                numero: "2311.2342.5235.4138",
                cvv: "123",
                validade: "2024-01-01",
                titular: "teste",
                valor: 100
            }
        ];

        const results = [];

        for (const cartao of cartoes) {
            results.push(pagamentoService.criarTransacao(cartao));
        }

        const pagamentos = await Promise.all(results);

        for (const pagamento of pagamentos) {
            expect(pagamento.success).not.toBeTruthy();
            expect(pagamento.code).not.toBe(-1);
        }
    }, 30000);

    /**
     * O formato válido é YYYY-MM-DD.
     */
    test("Deve falhar ao criar uma transação se o formato da data estiver inválido.", async () => {
        const cartao = {
            numero: "2311.2342.5235.4132",
            cvv: "123",
            validade: "01/2024",
            titular: "teste",
            valor: 100
        };

        const pagamento = await pagamentoService.criarTransacao(cartao);

        expect(pagamento.success).not.toBeTruthy();
        expect(pagamento.code).toBe(-1);
    }, 30000);
});
