const { MailjetService, DotenvService } = require("../../../src/services/index.js");

describe("MailjetService", () => {
    let dotenvService = null;
    let mailjetService = null;

    beforeEach(() => {
        dotenvService = new DotenvService();
        mailjetService = new MailjetService({ privateKey : dotenvService.get("MAILJET_PRIVATE_KEY"), publicKey: dotenvService.get("MAILJET_PUBLIC_KEY") });
    });

    test("Deve enviar um email com sucesoo.", async () => {
        await expect(mailjetService.enviar({ 
            from: dotenvService.get("MAIL_SENDER"), 
            to: dotenvService.get("MAIL_RECEIVER"), 
            subject: "aa", 
            message: "aa" 
        })).resolves.not.toThrow();
    }, 3000);

    test("Deve falhar ao passar um destinatário vazio.", async () => {
        await expect(mailjetService.enviar({ 
            from: dotenvService.get("MAIL_SENDER"), 
            to: "", 
            subject: "aa", 
            message: "aa" 
        })).rejects.toThrow();
    }, 3000)
})
