
const request = require("supertest");
const server = require("../../../src/server.js")

describe("CartaoController", () => {
    describe("POST /validaCartaoDeCredito", () => {
        test("Deve validar um cartão com com sucesso.", async () => {
            // Arrange & Act
            const response = await request(server)
                .post("/validaCartaoDeCredito")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ nomeTitular: "joao", cvv: "123", numero: "0000000000000002", validade: "2024-01-01" });

            
            // Assert
            expect(response.status).toBe(200);
            expect(response.body).toEqual({});
        });

        test("Deve falhar se o cartão não for válido.", async () => {
            // Arrange & Act
            const response = await request(server)
                .post("/validaCartaoDeCredito")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ nomeTitular: "joao", cvv: "123", numero: "2184284119341935", validade: "2024-01-01" });

            // Assert
            expect(response.status).toBe(422);
        });
    });
});
