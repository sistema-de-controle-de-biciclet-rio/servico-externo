
const request = require("supertest");
const server = require("../../../src/server.js")

describe("EmailController", () => {
    describe("POST /enviarEmail", () => {
        test("Deve enviar um email com sucesso.", async () => {
            // Arrange & Act
            const response = await request(server)
                .post("/enviarEmail")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ assunto: "Teste de Integração", email: "joaovitorsouzacoura@gmail.com", mensagem: "Teste de integração executou com sucesso." });

            // Assert
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty("email", "joaovitorsouzacoura@gmail.com");
            expect(response.body).toHaveProperty("assunto", "Teste de Integração");
            expect(response.body).toHaveProperty("mensagem", "Teste de integração executou com sucesso.");
        });

        test("Deve falhar se o formato do email for inválido.", async () => {
            // Arrange & Act
            const response = await request(server)
                .post("/enviarEmail")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ assunto: "Teste de Integração", email: "emailinvalido", mensagem: "Teste de integração executou com sucesso." });

            // Assert
            expect(response.status).toBe(422);
            
        }); 
    });
});
