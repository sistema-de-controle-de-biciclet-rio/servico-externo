const request = require("supertest");
const server = require("../../../src/server.js")

describe("CobrancaController", () => {

    describe("POST /cobranca", () => {
        test("Deve realizar uma cobraça com sucesso.", async () => {
            // Arrange & Act
            const response = await request(server)
                .post("/cobranca")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ valor: 100, ciclista: 5 });

            // Assert
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty("status", "PAGA");
            expect(response.body).toHaveProperty("valor", 100);
            expect(response.body).toHaveProperty("ciclista", 5);
        });
    });
    
    describe("GET /cobranca/:idCobranca", () => {
        test("Deve obter uma cobrança com sucesso.", async () => {
            // Arrange & Act
            const response = await request(server)
                .get("/cobranca/1")
                .set("Accept", "application/json")

            // Expect
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty("status", "PAGA");
            expect(response.body).toHaveProperty("valor", 100);
            expect(response.body).toHaveProperty("ciclista", 5);
        });

        test("Deve falhar se a cobrança não existir.", async () => {
            const response = await request(server)
                .get("/cobranca/1000")
                .set("Accept", "application/json");

            expect(response.status).toBe(404);
            expect(response.body).toEqual({
                codigo: 404,
                mensagem: "Cobrança não encontrada."
            });
        });
    });

    describe("POST /filaCobranca", () => {
        test("Deve inserir uma cobrança na fila de cobrança.", async () => {
            // Arrange && Act
            const response = await request(server)
                .post("/filaCobranca")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ valor: 50, ciclista: 5 });

            // Assert
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty("valor", 50);
            expect(response.body).toHaveProperty("status", "FALHA");
            expect(response.body).toHaveProperty("ciclista", 5);
        })
    });

    describe("POST /processaCobrancasEmFila", () => {
        test("Deve processar a fila de cobranças com sucesso.", async () => {
            //Arrange && Act
            const response = await request(server)
                .post("/processaCobrancasEmFila")
                .set("Accept", "application/json");

            // Assert
            const sucessos = response.body;
            expect(response.status).toBe(200);
            expect(sucessos[0]).toHaveProperty("valor", 50);
            expect(sucessos[0]).toHaveProperty("ciclista", 5);
            expect(sucessos[0]).toHaveProperty("status", "PAGA");
        });

        test("Deve falhar caso não seja possível recuperar os dados de cartão de um cisclista.", async () => {
            // Arrange
            // O ciclista 1000 não foi cadastrado no microserviço de aluguel.
            await request(server)
                .post("/filaCobranca")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ valor: 100, ciclista: 1000 });

            // Act
            const response = await request(server)
                .post("/processaCobrancasEmFila")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")

            // Assert
            expect(response.status).toBe(422);
        });
    });

});
