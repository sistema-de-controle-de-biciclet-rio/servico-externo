const CieloService = require("../../../src/services/pagamento/cielo.service");
const { RequestService, TimeService } = require("../mocks/services");

describe("CieloService", () => {
    let cieloService;

    beforeAll(() => {
        cieloService = new CieloService({
            requestService: RequestService,
            timeService: TimeService
        });
    });

    describe("criarTransacao()", () => {
        test("Deve criar uma transação com sucesso.", async () => {
            // Arrange
            const payment = {
                ReturnCode: "4",
                ReturnMessage: "Sucesso"
            }

            RequestService.post.mockResolvedValue({
                success: true,
                data: {
                    Payment: payment 
                }
            });
            
            // Act
            const resultado = await cieloService.criarTransacao({ numero: "0000000000000001", validade: "2024-01-01", cvv: "123", titular: "Joao", valor: 100});

            // Assert
            expect(RequestService.post).toHaveBeenCalled();
            expect(TimeService.formatar).toHaveBeenCalled();
            expect(resultado.code).toBe(payment.ReturnCode);
            expect(resultado.message).toBe(payment.ReturnMessage);
            expect(resultado.success).toBeTruthy();
        });

        test("Deve falhar se houver um erro na requisição para a cielo.", async () => {
            // Arrange
            RequestService.post.mockResolvedValue({
                success: false,
                message: "Requisição falhou."
            });
            
            // Act
            const resultado = await cieloService.criarTransacao({ numero: "0000000000000001", validade: "2024-01-01", cvv: "123", titular: "Joao", valor: 100});

            // Assert
            expect(RequestService.post).toHaveBeenCalled();
            expect(TimeService.formatar).toHaveBeenCalled();
            expect(resultado.code).toBe(-1);
            expect(resultado.message).toBe("Requisição falhou.");
            expect(resultado.success).not.toBeTruthy();
            
        });

        test("Deve falhar caso o pagamento não tenha sido aprovado.", async () => {
            // Arrange
            const payment = {
                ReturnCode: "1",
                ReturnMessage: "Pagamento recusado"
            }

            RequestService.post.mockResolvedValue({
                success: true,
                data: {
                    Payment: payment 
                }
            });
            
            // Act
            const resultado = await cieloService.criarTransacao({ numero: "0000000000000001", validade: "2024-01-01", cvv: "123", titular: "Joao", valor: 100});

            // Assert
            expect(RequestService.post).toHaveBeenCalled();
            expect(TimeService.formatar).toHaveBeenCalled();
            expect(resultado.code).toBe(payment.ReturnCode);
            expect(resultado.message).toBe(payment.ReturnMessage);
            expect(resultado.success).not.toBeTruthy();
        });
    });
    
    describe("validarCartao()", () => {
        test("Deve validar o cartão com sucesso.", async () => {
            // Arrange
            RequestService.post.mockResolvedValue({
                success: true,
                data: {
                    Valid: true
                }
            });

            // Act && Assert
            await expect(cieloService.validarCartao({ numero: "0000000000000001", validade: "2024-01-01", cvv: "123", titular: "Joao", valor: 100})).resolves.not.toThrow();
        });

        test("Deve falhar se houver um erro na requisição enviada para a cielo.", async () => {
            // Arrange
            RequestService.post.mockResolvedValue({
                success: false,
            });

            // Act && Assert
            await expect(cieloService.validarCartao({ numero: "0000000000000001", validade: "2024-01-01", cvv: "123", titular: "Joao", valor: 100})).rejects.toThrow();
        });

        test("Deve falhar se o cartão não for válido.", async () => {
            // Arrange
            RequestService.post.mockResolvedValue({
                success: true,
                data: {
                    Valid: false
                }
            });

            // Act && Assert
            await expect(cieloService.validarCartao({ numero: "0000000000000001", validade: "2024-01-01", cvv: "123", titular: "Joao", valor: 100})).rejects.toThrow();
            
        });
    }) 
});