const { AxiosService } = require("../../../src/services");

describe("AxiosService", () => {
    let axiosService;
    const mock = jest.fn();

    beforeAll(() => {
        axiosService = new AxiosService(mock);
    });
    
    describe("request()", () => {
        test("Deve fazer uma requisição com sucesso.", async () => {
            // Arrange
            mock.mockResolvedValue({ status: 200, data: "dados" });

            // Act
            const response = await axiosService.request("/", {});

            // Assert
            expect(response.success).toBeTruthy();
            expect(response.status).toBe(200);
            expect(response.data).toBe("dados");
        });

        test("Defalhar falhar se a requisição falhar.", async () => {
            // Arrange
            mock.mockImplementation(() => {
                throw Error("Erro na requisição.");
            });

            // Act
            const response = await axiosService.request("/", {});

            // Assert
            expect(response.success).not.toBeTruthy();
            expect(response.message).toBe("Erro na requisição.");
        });
    });
});