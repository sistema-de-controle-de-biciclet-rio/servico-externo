const { Tabela } = require("../../../src/infra/index.js");
const { Entidade } = require("../mocks/entities.js");

describe("Tabela", () => {
    let tabela = {};

    beforeEach(() => {
        const schema =  {
            id: {
                tipo: "number",
                geradoAutomaticamente: true
            },
            nome: {
                tipo: "string",
                permiteNulo: false
            },
            descricao: {
                tipo: "string",
                permiteNulo: true
            }
        };
        
        tabela = new Tabela(Entidade, schema);
    });

    describe("inserir()", () => {
        test("Deve inserir um registro no banco com sucesso.", async () => {
            // Arrange
            const dados = {
                nome: "Joao",
                descricao: "desenvolvedor"
            };

            // Act
            await tabela.inserir(dados);

            // Assert
            const registros = await tabela.todos({ nome: "Joao" });

            expect(registros.length).toBe(1);
            expect(registros[0].nome).toBe("Joao");
            expect(registros[0].descricao).toBe("desenvolvedor");
        });

        test("Deve falhar se o dado inserido não respeita o schema da tabela.", async () => {
            // Arrange 
            const dados1 = {
                nome: "João",
                descricao: 1
            };

            const dados2 = {
                id: 1,
                nome: "Joao",
                descricao: "desenvolvedor"
            };

            const dados3 = {
                descricao: "desenvolvedor"
            };

            // Act && Assert
            await expect(tabela.inserir(dados1)).rejects.toThrow();
            await expect(tabela.inserir(dados2)).rejects.toThrow();
            await expect(tabela.inserir(dados3)).rejects.toThrow();
        });
    });

    describe("atualizar()", () => {
        test("Deve atualizar um registro com sucesso.", async () => {
            // Arrange 
            const dados = {
                nome: "Joao",
                descricao: "desenvolvedor"
            };
            
            await tabela.inserir(dados);
            
            const dadosParaAtualizar = {
                nome: "Joao",
                descricao: "desenvolvedor junior"
            };

            // Act
            await tabela.atualizar(1, dadosParaAtualizar);

            // Assert
            const registro = await tabela.todos({ nome: "Joao" });
            
            expect(registro[0].nome).toBe("Joao");
            expect(registro[0].descricao).toBe("desenvolvedor junior");
        });

        test("Deve atualizar um registro, mesmo que somente parte dos dados tenham sido enviados.", async () => {
            // Arrange 
            const dados = {
                nome: "Joao",
                descricao: "desenvolvedor"
            };
            
            await tabela.inserir(dados);
            
            const dadosParaAtualizar = {
                descricao: "desenvolvedor pleno"
            };

            // Act
            await tabela.atualizar(1, dadosParaAtualizar);

            // Assert
            const registro = await tabela.todos({ nome: "Joao" });
            
            expect(registro[0].nome).toBe("Joao");
            expect(registro[0].descricao).toBe("desenvolvedor pleno");
        });

        test("Deve falhar ao atualizar se os dados não respeitarem o schema da tabela.", async () => {
            // Arrange 
            const dados = {
                nome: "Joao",
                descricao: "dev junior"
            };
            
            await tabela.inserir(dados);

            // Act && Assert
            await expect(tabela.atualizar(1, { descricao: 1 })).rejects.toThrow();
            await expect(tabela.atualizar(1, { id: 2 })).rejects.toThrow();
        });

        test("Deve falhar se o registro a ser atualizado não existir.", async () => {
            // Act && Assert
            await expect(tabela.atualizar(1, { nome: "Pedro" })).rejects.toThrow();
        });
    });

    describe("remover()", () => {
        test("Deve remover um registro com sucesso.", async () => {
            // Arrange 
            const dados = {
                nome: "Joao",
                descricao: "desenvolvedor"
            };

            await tabela.inserir(dados);

            // Act
            await tabela.remover(1);

            // Assert
            const registro = await tabela.todos({ nome: "Joao" });

            expect(registro.length).toBe(0);
        });
    });

    describe("salvar()", () => {
        test("Deve salvar uma entidade com sucesso", async () => {
            // Arrange 
            const dados = {
                nome: "Joao",
                descricao: "desenvolvedor"
            };

            await tabela.inserir(dados);

            const entidade = (await tabela.todos({ nome: "Joao" }))[0];
            entidade.nome = "Pedro";

            // Act
            await tabela.salvar(entidade);

            // Assert
            const registros = await tabela.todos();

            expect(registros[0].nome).toBe("Pedro");
            expect(registros[0].descricao).toBe("desenvolvedor");
        });
        
        test("Deve falhar se a entidade não respeitar o schema da tabela.", async () => {
            // Arrange 
            const dados = {
                nome: "Joao",
                descricao: "desenvolvedor"
            };

            await tabela.inserir(dados);

            const entidade = (await tabela.todos({ nome: "Joao" }))[0];
            entidade.nome = 1;

            // Act && Assert
            await expect(tabela.salvar(entidade)).rejects.toThrow();
        });
    });

    describe("todos()", () => {
        test("Deve recuperar todos os registros da tabela.", async () => {
            // Arrange 
            const dados = [
                {
                    nome: "Joao",
                    descricao: "desenvolvedor"
                },
                {
                    nome: "Pedro",
                    descricao: "QA"
                },
                {
                    nome: "Vitor",
                    descricao: "marketing"
                }
            ];
            
            const insercoes = [];

            dados.forEach(dado => insercoes.push(tabela.inserir(dado)));
            
            await Promise.all(insercoes);

            // Act
            const registros = await tabela.todos();

            // Assert
            expect(registros.length).toBe(3);
            expect(registros[0].nome).toBe("Joao");
            expect(registros[1].nome).toBe("Pedro");
            expect(registros[2].nome).toBe("Vitor");
        });

        test("Deve recuperar todos os registros conforme o filtro informado", async () => {
            // Arrange 
            const dados = [
                {
                    nome: "Joao",
                    descricao: "desenvolvedor"
                },
                {
                    nome: "Pedro",
                    descricao: "desenvolvedor"
                },
                {
                    nome: "Vitor",
                    descricao: "marketing"
                }
            ];
            
            const insercoes = [];

            dados.forEach(dado => insercoes.push(tabela.inserir(dado)));
            
            await Promise.all(insercoes);

            // Act
            const filtro1 = await tabela.todos({ descricao: "desenvolvedor" });
            const filtro2 = await tabela.todos({ nome: "Vitor", descricao: "marketing" });
            const filtro3 = await tabela.todos({ nome: "Alexandre" }); 

            // Assert
            expect(filtro1.length).toBe(2);
            expect(filtro1[0].nome).toBe("Joao");
            expect(filtro1[1].nome).toBe("Pedro");
            
            expect(filtro2.length).toBe(1);
            expect(filtro2[0].nome).toBe("Vitor");

            expect(filtro3.length).toBe(0);
        });
    });

    describe("unicoPorId()", () => {
        test("Deve retornar um unico registro com sucesso.", async () => {
            // Arrange 
            const dados = [
                {
                    nome: "Joao",
                    descricao: "desenvolvedor"
                },
                {
                    nome: "Pedro",
                    descricao: "desenvolvedor"
                },
                {
                    nome: "Vitor",
                    descricao: "marketing"
                }
            ];
            
            const insercoes = [];

            dados.forEach(dado => insercoes.push(tabela.inserir(dado)));
            
            await Promise.all(insercoes);

            // Act
            const registro = await tabela.unicoPorId(3);

            // Assert
            expect(registro.length).toBe(1);
            expect(registro[0].nome).toBe("Vitor");
        });

        test("Deve retornar uma colecao vazia caso nenhum registro seja encontrado.", async () => {
            // Arrange 
            const dados = [
                {
                    nome: "Joao",
                    descricao: "desenvolvedor"
                },
            ];
            
            const insercoes = [];

            dados.forEach(dado => insercoes.push(tabela.inserir(dado)));
            
            await Promise.all(insercoes);
            
            // Act
            const registro = await tabela.unicoPorId(3);

            // Assert
            expect(registro.length).toBe(0);
        });
    });

    describe("ultimoRegistroInserido()", () => {
        test("Deve recuperar o último registro inserido no banco", async () => {
            // Arrange 
            const dados = [
                {
                    nome: "Joao",
                    descricao: "desenvolvedor"
                },
                {
                    nome: "Pedro",
                    descricao: "desenvolvedor"
                }
            ];
            
            const insercoes = [];

            dados.forEach(dado => insercoes.push(tabela.inserir(dado)));
            
            await Promise.all(insercoes);
            
            // Act
            const registro = await tabela.ultimoRegistroInserido();

            // Assert
            expect(registro.length).toBe(1);
            expect(registro[0].nome).toBe("Pedro");
        });

        test("Deve falhar caso nenhum registro tenha sido inserido no banco.", async () => {
            // Act & Assert
            await expect(tabela.ultimoRegistroInserido()).rejects.toThrow();
        });
    });
});
