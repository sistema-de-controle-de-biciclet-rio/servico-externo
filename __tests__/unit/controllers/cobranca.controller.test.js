const { CobrancaController } = require("../../../src/controllers/index.js");
const { 
    Request, 
    Response,
    Next
} = require("../mocks/http.js");
const {
    DotenvService,
    TimeService,
    PagamentoService,
    RequestService
} = require("../mocks/services.js");
const {
    DataSource,
    Tabela
} = require("../mocks/banco-de-dados.js");
const {
    FilaEmail,
    FilaCobranca
} = require("../mocks/filas.js");
const { Cobranca } = require("../mocks/entities.js");

describe("CobrancaController", () => {
    let cobrancaController = {};
    let isEmpty = jest.fn().mockReturnValue(true);
    let array = jest.fn().mockReturnValue([]);
    let validation = jest.fn().mockReturnValue({ array, isEmpty });

    beforeAll(() => {
        cobrancaController = new CobrancaController({ 
            filaCobranca: FilaCobranca,
            filaEmail: FilaEmail,
            pagamentoService: PagamentoService, 
            datasource: DataSource, 
            timeService: TimeService, 
            requestService: RequestService, 
            mailService: {}, 
            dotenvService: DotenvService,
            validationResult: validation 
        });
    });

    afterEach(() => {
        Tabela.inserir.mockReset();
        FilaEmail.add.mockClear();
        Response.status.mockClear();
        Response.json.mockClear();
        Next.mockClear();
        isEmpty.mockReturnValue(true);
    });

    describe("processarFilaCobranca()", () => {

        test("Deve processar fila de cobranca vazia.", async () => {
            // Act  
            await cobrancaController.processarFilaCobranca(Request, Response);
            
            // Assert
            expect(FilaCobranca.processar).toBeCalled();
            expect(FilaCobranca.resultado).toBeCalled();

            expect(DataSource.tabela).not.toBeCalledWith("cobranca");
            expect(Tabela.salvar).not.toBeCalled();

            expect(FilaEmail.processar).toBeCalled();
            expect(DataSource.tabela).toBeCalledWith("email");
            expect(Tabela.inserir).not.toBeCalled();
            expect(FilaEmail.add).not.toBeCalled();
            expect(Tabela.ultimoRegistroInserido).not.toBeCalled();
            
            expect(Response.status).toBeCalledWith(200);
            expect(Response.json).toBeCalledWith([]);
            expect(Response.status).not.toBeCalledWith(422);
        });

        test("Deve processar fila de cobrança sem falhas, mas com sucessos.", async () => {
            // Arrange 
            const cobrancaSucesso1 = Cobranca.new();
            const cobrancaSucesso2 = Cobranca.new();

            cobrancaSucesso1.convertidoParaHTML.mockReturnValue("<h1>Cobrança 1</h1>");
            cobrancaSucesso1.convertidoParaObjeto.mockReturnValue({ id: 1 });
            
            cobrancaSucesso2.convertidoParaObjeto.mockReturnValue({ id: 2 });
            cobrancaSucesso2.convertidoParaHTML.mockReturnValue("<h1>Cobrança 2</h1>");

            FilaCobranca.resultado.mockResolvedValue({ sucessos: [ cobrancaSucesso1, cobrancaSucesso2 ], falhas: [] });
            DotenvService.get.mockReturnValue("email@mock.com");
            Tabela.ultimoRegistroInserido
                .mockResolvedValueOnce(cobrancaSucesso1.convertidoParaObjeto())
                .mockResolvedValueOnce(cobrancaSucesso2.convertidoParaObjeto());

            // Act
            await cobrancaController.processarFilaCobranca(Request, Response);
           
            // Assert
            expect(FilaCobranca.processar).toBeCalled();
            expect(FilaCobranca.resultado).toBeCalled();
            
            expect(DotenvService.get).toBeCalledWith("MAIL_RECEIVER");
            expect(Tabela.inserir).toBeCalledTimes(2);
            expect(FilaEmail.add).toBeCalledTimes(2);
            expect(Tabela.inserir).toHaveBeenNthCalledWith(1, {
                email: "email@mock.com",
                mensagem: "<h1>Cobrança 1</h1>",
                assunto: "Cobrança em atraso"
            });
            expect(Tabela.inserir).toHaveBeenNthCalledWith(2, {
                email: "email@mock.com",
                mensagem: "<h1>Cobrança 2</h1>",
                assunto: "Cobrança em atraso"
            });
            expect(FilaEmail.add).toHaveBeenNthCalledWith(1, { id: 1 });
            expect(FilaEmail.add).toHaveBeenNthCalledWith(2, { id: 2 });

            expect(DataSource.tabela).toBeCalledWith("cobranca");
            expect(Tabela.salvar).toBeCalledWith(cobrancaSucesso1);
            expect(Tabela.salvar).toBeCalledWith(cobrancaSucesso2);

            expect(Response.status).toBeCalledWith(200);
            expect(Response.json).toBeCalledWith([ { id: 1 }, { id: 2 } ]);
        });
    
        test("Deve processar a fila de cobrança com falhas, mas sem sucessos.", async () => {
            // Arrange
            const cobrancaFalha1 = Cobranca.new();
            const cobrancaFalha2 = Cobranca.new();

            Object.defineProperty(cobrancaFalha1, "id", {
                get: jest.fn().mockReturnValue(1)
            });
            Object.defineProperty(cobrancaFalha2, "id", {
                get: jest.fn().mockReturnValue(2)
            });

            FilaCobranca.resultado.mockResolvedValue({ sucessos: [], falhas: [ cobrancaFalha1, cobrancaFalha2 ] });

            // Act
            await cobrancaController.processarFilaCobranca(Request, Response, Next);
            
            // Assert
            expect(DataSource.tabela).toBeCalledWith("email");
            expect(Tabela.inserir).not.toBeCalled();
            expect(FilaEmail.add).not.toBeCalled();
            expect(FilaEmail.processar).toBeCalled();

            expect(Response.status).not.toBeCalled();
            expect(Response.json).not.toBeCalled();
            expect(Next).toBeCalledWith([
                { codigo: 422, mensagem: `Falha ao processar cobrança de id 1.` },
                { codigo: 422, mensagem: `Falha ao processar cobrança de id 2.` }
            ]);
        });
    });
    
    describe("realizarCobranca()", () => {
        test("Deve realizar uma cobrança com sucesso.", async () => {
            // Arrange 
            const cobranca = Cobranca.new();
            cobranca.convertidoParaObjeto.mockReturnValue({ id: 1 });
            
            Tabela.ultimoRegistroInserido.mockResolvedValue([cobranca]); 
            
            TimeService.horaAtual.mockReturnValue("2022-01-01");

            Request.body = { valor: 100, ciclista: 1 };
            
            // Act
            await cobrancaController.realizarCobranca(Request, Response, Next);
   
            // Assert
            expect(DataSource.tabela).toBeCalledWith("cobranca");
            expect(TimeService.horaAtual).toBeCalled();
            expect(Tabela.inserir).toBeCalledWith({ valor: 100, ciclista: 1, status: "PENDENTE", horaSolicitacao: "2022-01-01", horaFinalizacao: "" });
            expect(Tabela.ultimoRegistroInserido).toBeCalled();
            expect(cobranca.cobrar).toBeCalledWith(RequestService, PagamentoService, TimeService);
            expect(Response.status).toBeCalledWith(200);
            expect(Response.json).toBeCalledWith({ id: 1 });
            expect(Next).not.toBeCalled();
        });

        test("Deve falhar caso os dados da requisição não sejam enviado.", async () => {
            // Arrange
            isEmpty.mockReturnValue(false);

            // Act
            await cobrancaController.realizarCobranca(Request, Response, Next);

            // Assert
            expect(array).toHaveBeenCalled();
            expect(Response.status).not.toHaveBeenCalled();
        });

        test("Deve falhar caso haja um erro ao realizar a cobranca.", async () => {
            // Arrange
            Request.body = { valor: 100, ciclista: 1 };
            Tabela.inserir.mockImplementation(() => {
                throw Error("Erro ao inserir cobrança no banco.");
            });

            // Act 
            await cobrancaController.realizarCobranca(Request, Response, Next);
    
            expect(Next).toBeCalledWith([{
                codigo: 422,
                mensagem: "Erro ao inserir cobrança no banco." 
            }]);
            expect(Response.status).not.toBeCalledWith(200);
            expect(Response.json).not.toBeCalled(); 
        });
    });

    describe("incluirCobrancaEmFilaCobranca()", () => {
        test("Deve incluir uma cobrança na fila de cobrança com sucesso.", async () => {
            // Arrange
            const cobranca = Cobranca.new();
            cobranca.convertidoParaObjeto.mockReturnValue({ id: 1 });

            Tabela.ultimoRegistroInserido.mockResolvedValue([cobranca]);
            TimeService.horaAtual.mockReturnValue("2022-01-01");
            Request.body = { valor: 100, ciclista: 1 };

            // Act
            await cobrancaController.incluirCobrancaEmFilaCobranca(Request, Response, Next);
            
            // Assert
            expect(TimeService.horaAtual).toBeCalled();
            expect(DataSource.tabela).toBeCalledWith("cobranca");
            expect(Tabela.inserir).toBeCalledWith({
                valor: 100,
                ciclista: 1,
                horaSolicitacao: "2022-01-01",
                horaFinalizacao: "",
                status: "FALHA"
            });
            expect(Tabela.ultimoRegistroInserido).toBeCalled();
            expect(FilaCobranca.add).toBeCalledWith(cobranca);
            expect(Response.status).toBeCalledWith(200);
            expect(Response.json).toBeCalledWith({ id: 1 });
            expect(Next).not.toBeCalled();
        });
        
        test("Deve falhar caso os dados da requisição não sejam enviado.", async () => {
            // Arrange
            isEmpty.mockReturnValue(false);

            // Act
            await cobrancaController.incluirCobrancaEmFilaCobranca(Request, Response, Next);

            // Assert
            expect(array).toHaveBeenCalled();
            expect(Response.status).not.toHaveBeenCalled();
        });

        test("Deve falhar se houver qualquer erro ao incluir a cobrança na fila de cobrança.", async () => {
            // Arrange
            Request.body = { valor: 100, ciclista: 1 };
            Tabela.inserir.mockImplementation(() => {
                throw Error("Erro ao inserir cobrança no banco.");
            });

            // Act
            await cobrancaController.incluirCobrancaEmFilaCobranca(Request, Response, Next);

            // Assert
            expect(Next).toBeCalledWith([{
                codigo: 422,
                mensagem: "Erro ao inserir cobrança no banco."
            }]);
            expect(Response.json).not.toBeCalled();
            expect(Response.status).not.toBeCalled();
        });
    });


    describe("obterCobranca()", () => {
        test("Deve obter uma cobranca com sucesso.", async () => {
            // Arrange
            Request.params = { idCobranca: 1 };
            const cobranca = Cobranca.new();
            cobranca.convertidoParaObjeto.mockReturnValue({ id: 1 });
            Tabela.unicoPorId.mockResolvedValue([cobranca]);

            // Act
            await cobrancaController.obterCobranca(Request, Response, Next); 
           
            // Assert
            expect(DataSource.tabela).toBeCalledWith("cobranca");
            expect(Tabela.unicoPorId).toBeCalledWith(Request.params.idCobranca);
            expect(Next).not.toBeCalled(); 
            expect(Response.status).toBeCalledWith(200);
            expect(Response.json).toBeCalled();
        });
        
        test("Deve falhar caso os dados da requisição não sejam enviado.", async () => {
            // Arrange
            isEmpty.mockReturnValue(false);

            // Act
            await cobrancaController.obterCobranca(Request, Response, Next);

            // Assert
            expect(array).toHaveBeenCalled();
            expect(Response.status).not.toHaveBeenCalled();
        });

        test("Deve falhar se a cobrança sendo buscada não for encontrada.", async () => {
            // Arrange
            Request.params = { idCobranca: 1 };
            Tabela.unicoPorId.mockResolvedValue([]);

            // Act
            await cobrancaController.obterCobranca(Request, Response, Next);
            
            // Assert
            expect(Next).toBeCalledWith({
                codigo: 404,
                mensagem: "Cobrança não encontrada."
            });
            expect(Response.json).not.toBeCalled();
            expect(Response.status).not.toBeCalled();
        });
    });
});
