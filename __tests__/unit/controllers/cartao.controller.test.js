const { CartaoController } = require("../../../src/controllers/index.js");

const { DataSource, Tabela } = require("../mocks/banco-de-dados.js");
const { Cartao } = require("../mocks/entities.js");
const { PagamentoService } = require("../mocks/services.js");
const { Request, Response, Next } = require("../mocks/http.js");

describe("CartaoController", () => {
    let controller = {};
    let isEmpty = jest.fn().mockReturnValue(true);
    let array = jest.fn().mockReturnValue([]);
    let validation = jest.fn().mockReturnValue({ array, isEmpty });

    beforeAll(() => {
        controller = new CartaoController({ datasource: DataSource, pagamentoService: PagamentoService, validationResult: validation });
    });

    beforeEach(() => {
        isEmpty.mockReturnValue(true);
        array.mockReturnValue([]);
        Response.end.mockReset();
    });

    describe("validarCartaoDeCredito", () => {
        test("Deve validar um cartão de crédito com sucesso.", async () => {
            // Arrange
            const cartao = Cartao.new();
            Tabela.ultimoRegistroInserido.mockResolvedValue([cartao]);
            Request.body = { nomeTitular: "titular", cvv: "123", numero: "0000000000000001", validade: "2024-01-01" };
            
            // Act
            await controller.validarCartaoDeCredito(Request, Response, Next);

            // Assert
            expect(DataSource.tabela).toHaveBeenCalledWith("cartao");
            expect(Tabela.inserir).toHaveBeenCalledWith(Request.body);
            expect(Tabela.ultimoRegistroInserido).toHaveBeenCalled();
            expect(cartao.validar).toHaveBeenCalledWith(PagamentoService);
            expect(Response.status).toHaveBeenCalledWith(200);
            expect(Response.end).toHaveBeenCalled();
            expect(Next).not.toHaveBeenCalled();
        });

        test("Deve falhar se os dados da requisição não forem enviados corretamente.", async () => {
            // Arrange
            isEmpty.mockReturnValue(false);

            // Act
            await controller.validarCartaoDeCredito(Request, Response, Next);

            // Assert
            expect(array).toHaveBeenCalled();
            expect(Next).toHaveBeenCalled();
            expect(Response.end).not.toHaveBeenCalled();

        });

        test("Deve falhar se a validação falhar.", async () => {
            // Arrange
            const cartao = Cartao.new();
            Tabela.ultimoRegistroInserido.mockResolvedValue([cartao]);
            Request.body = { nomeTitular: "titular", cvv: "123", numero: "0000000000000001", validade: "2024-01-01" };
            cartao.validar.mockImplementation(() => {
                throw Error("Cartão inválido.");
            });

            // Act
            await controller.validarCartaoDeCredito(Request, Response, Next);

            // Assert
            expect(DataSource.tabela).toHaveBeenCalledWith("cartao");
            expect(Tabela.inserir).toHaveBeenCalledWith(Request.body);
            expect(Tabela.ultimoRegistroInserido).toHaveBeenCalled();
            expect(cartao.validar).toHaveBeenCalledWith(PagamentoService);
            expect(Next).toHaveBeenCalledWith([{ codigo: 422, mensagem: "Cartão inválido." }]);
        });
    });
});
