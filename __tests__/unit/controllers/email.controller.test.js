const { EmailController } = require("../../../src/controllers/index.js");

const { Email } = require("../mocks/entities.js");

const {
    MailService,
    DotenvService
} = require("../mocks/services.js");

const {
    DataSource,
    Tabela,
} = require("../mocks/banco-de-dados.js");

const { 
    Request,
    Response,
    Next
} = require("../mocks/http.js");

describe("EmailController", () => {
    let emailController = {};
    
    let isEmpty = jest.fn().mockReturnValue(true);
    let array = jest.fn().mockReturnValue([]);
    let validation = jest.fn().mockReturnValue({ array, isEmpty });

    beforeAll(() => {
        emailController = new EmailController({ 
            mailService: MailService,
            dotenvService: DotenvService,
            datasource: DataSource,
            validationResult: validation
        });
    });

    beforeEach(() => {
        Tabela.inserir.mockReset();
        Tabela.ultimoRegistroInserido.mockReset();
        Response.json.mockClear();
        Response.status.mockClear();
        DataSource.tabela.mockClear();
        isEmpty.mockReturnValue(true);
        Next.mockReset();
    });

    test("Deve enviar um email com sucesso.", async () => {
        // Arrange
        const body = { email: "teste@mail.com", assunto: "assunto", mensagem: "mensagem" };
        Request.body = body;

        const email = Email.new();
        email.valido.mockReturnValue(true);
        email.convertidoParaObjeto.mockReturnValue({ id: 1 });
        
        Tabela.ultimoRegistroInserido.mockResolvedValue([email]);

        DotenvService.get.mockReturnValue("sender@mail.com");

        // Act
        await emailController.enviarEmail(Request, Response, Next);

        // Assert
        expect(DataSource.tabela).toBeCalledWith("email");
        expect(Tabela.inserir).toBeCalledWith(body);
        expect(Tabela.ultimoRegistroInserido).toBeCalled();
        expect(email.valido).toBeCalled();
        expect(email.enviar).toBeCalledWith("sender@mail.com", MailService);
        expect(Next).not.toBeCalledWith({ codigo: 422, mensagem: "E-mail com formato inválido." });
        expect(Response.status).toBeCalledWith(200);
        expect(Response.json).toBeCalledWith({ id: 1 });
    });

    test("Deve falhar se os dados da função não forem enviados corretamente.", async () => {
        // Arrange
        isEmpty.mockReturnValue(false); 
        
        // Act
        await emailController.enviarEmail(Request, Response, Next);

        // Assert
        expect(isEmpty).toHaveBeenCalled();
        expect(array).toHaveBeenCalled();
        expect(Response.json).not.toHaveBeenCalled();
    });

    test("Deve falhar quando o email não possui um formato válido.", async () => {
        // Arrange
        Request.body = {
            email: "email inválido",
            assunto: "assunto",
            mensagem: "mensagem"
        };

        const email = Email.new();
        email.valido.mockReturnValue(false);

        Tabela.ultimoRegistroInserido.mockResolvedValue([email]);

        // Act
        await emailController.enviarEmail(Request, Response, Next);
        
        // Assert
        expect(Response.status).not.toBeCalledWith(200);
        expect(Response.json).not.toBeCalled();
        expect(Next).toBeCalledWith([{
            codigo: 422,
            mensagem: "E-mail com formato inválido."
        }]);
    });

    test("Deve falhar quando houver um erro no envio do email.", async () => {
        // Arrange
        DataSource.tabela.mockImplementation(() => {
            throw Error("E-mail não existe.");
        });
        
        // Act
        await emailController.enviarEmail(Request, Response, Next);
        
        // Assert
        expect(Tabela.inserir).not.toBeCalled()
        expect(Response.status).not.toBeCalled();
        expect(Response.json).not.toBeCalled();
        expect(Next).toBeCalledWith({
            codigo: 404,
            mensagem: "E-mail não existe."
        });
    });
});
