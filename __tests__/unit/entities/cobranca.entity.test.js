const { Cobranca } = require("../../../src/entities/index.js");
const { DotenvService } = require("../../../src/services/index.js");

const { 
    RequestService,
    PagamentoService,
    TimeService,
} = require("../mocks/services.js");


describe("Cobranca", () => {
    describe("constructor()", () => {
        test("Deve criar um cobrança.", () => {
            const cobranca = new Cobranca({ valor: 100, status: "PAGA", horaSolicitacao: "2024-01-01 10:00:00", horaFinalizacao: "2024-01-01 10:00:00", ciclista: 1 });

            expect(cobranca.valor).toBe(100);
            expect(cobranca.status).toBe("PAGA");
            expect(cobranca.horaSolicitacao).toBe("2024-01-01 10:00:00");
            expect(cobranca.horaFinalizacao).toBe("2024-01-01 10:00:00");
            expect(cobranca.ciclista).toBe(1);
        });
    });

    describe("cobrar()", () => {
        let cobranca = {};

        const dadosDoCartao = [{
            nomeTitular: "Titular",
            numero: "0000.0000.0000.0000",
            validade: "2027-01-27",
            cvv: "124"
        }];

        const dadosDaCobranca = {
            id: 1,
            status: "Pendente",
            horaSolicitacao: "2024-01-01 12:00:00",
            horaFinalizacao: "",
            valor: 100,
            ciclista: 1
        };

        beforeEach(() => {
            cobranca = new Cobranca(dadosDaCobranca);
            PagamentoService.criarTransacao.mockClear();
        });

        test("Deve realizar um cobrança com sucesso.", async () => {
            // Arrange
            RequestService.get.mockResolvedValue({
                success: true, 
                status: 200,
                data: dadosDoCartao
            });

            TimeService.horaAtual.mockReturnValue("2024-01-02 12:00:00");
            
            PagamentoService.criarTransacao.mockResolvedValue({ success: true });

            // Act
            await cobranca.cobrar(RequestService, PagamentoService, TimeService);

            const dotenv = new DotenvService();
            const url = dotenv.get("ALUGUEL_URL");

            // Assert
            expect(RequestService.get).toBeCalledWith(`${url}/cartaoDeCredito/${dadosDaCobranca.ciclista}`);
            expect(PagamentoService.criarTransacao).toBeCalledWith({
                cvv: dadosDoCartao[0].cvv,
                numero: dadosDoCartao[0].numero,
                titular: dadosDoCartao[0].nomeTitular,
                validade: dadosDoCartao[0].validade,
                valor: dadosDaCobranca.valor
            });
            expect(TimeService.horaAtual).toBeCalled();
            expect(cobranca.status).toBe("PAGA");
            expect(cobranca.horaFinalizacao).toBe("2024-01-02 12:00:00");
        });

        test("Deve falhar se houver um erro ao recuperar os dados do ciclista.", async () => {
            // Arrange
            RequestService.get.mockResolvedValue({
                success: false, 
            });
            
            // Act && Assert
            await expect(cobranca.cobrar(RequestService, PagamentoService, TimeService)).rejects.toThrow();

            const dotenv = new DotenvService();
            const url = dotenv.get("ALUGUEL_URL");

            expect(RequestService.get).toBeCalledWith(`${url}/cartaoDeCredito/${dadosDaCobranca.ciclista}`);
            expect(PagamentoService.criarTransacao).not.toHaveBeenCalled();
            expect(cobranca.status).toBe("FALHA");
        });
        
        test("Deve falhar se houver um erro ao criar a transação.", async () => {
            // Arrange
            RequestService.get.mockResolvedValue({
                success: true, 
                data: dadosDoCartao
            });
            
            PagamentoService.criarTransacao.mockResolvedValue({ success: false });
            
            // Act && Assert
            await expect(cobranca.cobrar(RequestService, PagamentoService, TimeService)).rejects.toThrow();

            const dotenv = new DotenvService();
            const url = dotenv.get("ALUGUEL_URL");

            expect(RequestService.get).toBeCalledWith(`${url}/cartaoDeCredito/${dadosDaCobranca.ciclista}`);
            expect(PagamentoService.criarTransacao).toHaveBeenCalled();
            expect(cobranca.status).toBe("FALHA");
        });
    });

    describe("convertidoParaObjeto()", () => {
        test("Deve retornar a cobrança convertida para objeto.", async () => {
            const dados =  { 
                id: 1, 
                valor: 100, 
                status: "Pendente", 
                horaSolicitacao: "2024-01-01 12:00:00", 
                horaFinalizacao: "", 
                ciclista: 1 
            };

            const cobranca = new Cobranca(dados);

            // Act 
            const objeto = cobranca.convertidoParaObjeto();

            // Assert
            expect(objeto).toEqual(dados);
        });
    });
});
