const { Email } = require("../../../src/entities/index.js");

const { MailService } = require("../mocks/services.js");

describe("Email", () => {
    describe("enviar()", () => {
        test("Deve enviar um email com sucesso.", async () => {
            // Arrange 
            const dados = {
                id: 1,
                email: "user@mail.com",
                mensagem: "aaaa",
                assunto: "AAAA"
            };

            const email = new Email(dados);

            // Act
            await email.enviar("remetente@mail.com", MailService);

            // Assert
            expect(MailService.enviar).toBeCalledWith({ from: "remetente@mail.com", to: dados.email, subject: dados.assunto, message: dados.mensagem });
        });
    });

    describe("valido()", () => {
        test("Deve validar o email.", () => {
            // Arrange 
            const dadosDoEmailValido = {
                id: 1,
                email: "user@mail.com",
                mensagem: "aaaa",
                assunto: "AAAA"
            };

            const emailValido = new Email(dadosDoEmailValido);
            
            const dadosDoEmailInvalido1 = {
                id: 2,
                email: "emailinvalido",
                mensagem: "aaaa",
                assunto: "AAAA"
            };

            const emailInvalido1 = new Email(dadosDoEmailInvalido1);

            const dadosDoEmailInvalido2 = {
                id: 3,
                email: "@mail.com",
                mensagem: "aaaa",
                assunto: "AAAA"
            };

            const emailInvalido2 = new Email(dadosDoEmailInvalido2);

            // Act && Assert
            expect(emailValido.valido()).toBeTruthy();
            expect(emailInvalido1.valido()).not.toBeTruthy();
            expect(emailInvalido2.valido()).not.toBeTruthy();
        });
    });

    describe("convertidoParaObjeto()", () => {
        test("Deve retornar o email convertido para objeto.", () => {
            // Arrange 
            const dados = { id: 1, assunto: "assunto", email: "test@mail.com", mensagem: "mensagem" };
            const email = new Email(dados);
            
            // Act
            const objeto = email.convertidoParaObjeto();

            // Assert
            expect(objeto).toEqual(dados);
        });
    });
});
