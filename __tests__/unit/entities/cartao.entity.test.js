const { Cartao } = require("../../../src/entities/index.js");
const { PagamentoService } = require("../mocks/services.js");

describe("Cartao", () => {
    describe("validar()", () => {
        test("Deve validar o cartão com sucesso.", async () => {
            // Arrange
            const dados = { nomeTitular: "joao", numero: "0000000000000000", validade: "2024-01-01", cvv: "123" };
            const cartao = new Cartao(dados);

            // Act && Assert
            await expect(cartao.validar(PagamentoService)).resolves.not.toThrow();
            expect(PagamentoService.validarCartao).toHaveBeenCalledWith(dados);
        });

        test("Deve falhar se o numero do cartão for inválido.", async () => {
            // Arrange
            const dados = { nomeTitular: "joao", numero: "000000000000", validade: "2024-01-01", cvv: "123" };
            const cartao = new Cartao(dados);

            // Act && Assert
            await expect(cartao.validar(PagamentoService)).rejects.toThrow("Número do cartão inválido: deve estar no formato 9999999999999999.");
            expect(PagamentoService.validarCartao).not.toHaveBeenCalledWith(dados);
        });
        
        test("Deve falhar se o cvv do cartão for inválido.", async () => {
            // Arrange
            const dados = { nomeTitular: "joao", numero: "0000000000000000", validade: "2024-01-01", cvv: "23" };
            const cartao = new Cartao(dados);

            // Act && Assert
            await expect(cartao.validar(PagamentoService)).rejects.toThrow("CVV inválido: deve estar no formato 999.");
            expect(PagamentoService.validarCartao).not.toHaveBeenCalledWith(dados);
        });
        
        test("Deve falhar se validade do cartão for inválida.", async () => {
            // Arrange
            const dados = { nomeTitular: "joao", numero: "0000000000000000", validade: "20240101", cvv: "123" };
            const cartao = new Cartao(dados);

            // Act && Assert
            await expect(cartao.validar(PagamentoService)).rejects.toThrow("Validade inválida: deve estar no formato 9999-99-99");
            expect(PagamentoService.validarCartao).not.toHaveBeenCalledWith(dados);
        });
    });
});
