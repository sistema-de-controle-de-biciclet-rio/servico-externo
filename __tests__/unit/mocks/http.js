const Response = {
    json: jest.fn(() => Response),
    status: jest.fn(() => Response),
    end: jest.fn(() => Response)
};

const Request = {
    body: {},
    params: {}
};

const Next = jest.fn();

module.exports = { Request, Response, Next };
