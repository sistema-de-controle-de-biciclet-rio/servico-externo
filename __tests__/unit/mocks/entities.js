class Entidade {
    constructor({ id, nome, descricao }) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
    }
}

const Cobranca = {
    new: (dados) => {
        return {
            ...dados,
            cobrar: jest.fn(),
            convertidoParaHTML: jest.fn(),
            convertidoParaObjeto: jest.fn(),
            igual: jest.fn((outro) => dados._id === outro._id)
        }
    }
};

const Email = {
    new: () => {
        return {
            valido: jest.fn(),
            convertidoParaHTML: jest.fn(),
            convertidoParaObjeto: jest.fn(),
            enviar: jest.fn(),
            igual: jest.fn()
        }
    }
};

const Cartao = {
    new: () => {
        return {
            validar: jest.fn()
        }
    }
}

module.exports = { Entidade, Cobranca, Email, Cartao };
