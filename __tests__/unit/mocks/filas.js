const FilaCobranca = {
    processar: jest.fn(),
    resultado: jest.fn(() => {
        return { sucessos: [], falhas: [] };
    }),
    add: jest.fn(),
    limpar: jest.fn(),
    remover: jest.fn()
};

const FilaEmail = {
    processar: jest.fn(),
    resultado: jest.fn(() => {
        return { sucessos: [], falhas: [] };
    }),
    add: jest.fn(),
    limpar: jest.fn(),
    remover: jest.fn()
};

module.exports = {
    FilaCobranca,
    FilaEmail
};
