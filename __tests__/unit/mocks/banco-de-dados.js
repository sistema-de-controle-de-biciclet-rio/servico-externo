const Tabela = {
    inserir: jest.fn(),
    atualizar: jest.fn(),
    remover: jest.fn(),
    salvar: jest.fn(),
    ultimoRegistroInserido: jest.fn(),
    todos: jest.fn().mockResolvedValue([]),
    unicoPorId: jest.fn()
};

const DataSource = {
    tabela: jest.fn(() => Tabela)
};

module.exports = { DataSource, Tabela };
