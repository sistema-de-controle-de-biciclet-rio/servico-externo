const DotenvService = {
    get: jest.fn()
};

const TimeService = {
    horaAtual: jest.fn(),
    formatar: jest.fn()
};

const PagamentoService = {
    criarTransacao: jest.fn(),
    validarCartao: jest.fn()
};

const RequestService = {
    get: jest.fn(),
    post: jest.fn()
};

const MailService = {
    enviar: jest.fn()
};

module.exports = { 
    DotenvService,
    TimeService,
    RequestService,
    MailService,
    PagamentoService
};
