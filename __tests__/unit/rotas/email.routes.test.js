
const { RotasDeEmail } = require("../../../src/routes/index.js");
const { Datasource } = require("../mocks/banco-de-dados.js");

describe("RotasDeEmail", () => {
    test("Deve registrar as rotas de email.", () => {
        // Arrange
        const container = {
            get: jest.fn().mockReturnValue([])
        };

        const roteador = {
            get: jest.fn(),
            post: jest.fn()
        }

        const cobranca = new RotasDeEmail(container, Datasource);

        // Act
        cobranca.registrar(roteador);

        // Assert
        expect(container.get).toHaveBeenCalledWith(["MailService", "DotenvService"]);
        expect(roteador.post).toHaveBeenCalledWith("/enviarEmail", expect.anything(), expect.anything(), expect.anything(), expect.anything());
    });
});