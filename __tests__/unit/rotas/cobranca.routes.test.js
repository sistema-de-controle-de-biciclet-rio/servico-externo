const { RotasDeCobranca } = require("../../../src/routes/index.js");
const { DataSource } = require("../mocks/banco-de-dados.js");
const { TimeService } = require("../mocks/services.js");

describe("RotasDeCobranca", () => {
    test("Deve registrar as rotas de cobrança.", () => {
        // Arrange
        const container = {
            get: jest.fn().mockReturnValue([{}, {add: jest.fn()}, {}, TimeService])
        };

        const roteador = {
            get: jest.fn(),
            post: jest.fn()
        }

        const cobranca = new RotasDeCobranca(container, DataSource);

        // Act
        cobranca.registrar(roteador);

        // Assert
        expect(container.get).toHaveBeenCalledWith(["FilaEmail", "RequestService", "TimeService", "PagamentoService", "DotenvService"]);
        expect(roteador.get).toHaveBeenCalledWith("/cobranca/:idCobranca", expect.anything(), expect.anything());
        expect(roteador.post).toHaveBeenNthCalledWith(1, "/filaCobranca", expect.anything(), expect.anything(), expect.anything());
        expect(roteador.post).toHaveBeenNthCalledWith(2, "/cobranca", expect.anything(), expect.anything(), expect.anything());
        expect(roteador.post).toHaveBeenNthCalledWith(3, "/processaCobrancasEmFila", expect.anything());
    });
});