
const { RotasDeCartao } = require("../../../src/routes/index.js");
const { Datasource } = require("../mocks/banco-de-dados.js");

describe("RotasDeCartao", () => {
    test("Deve registrar as rotas de cartão.", () => {
        // Arrange
        const container = {
            get: jest.fn().mockReturnValue([])
        };

        const roteador = {
            get: jest.fn(),
            post: jest.fn()
        }

        const cobranca = new RotasDeCartao(container, Datasource);

        // Act
        cobranca.registrar(roteador);

        // Assert
        expect(container.get).toHaveBeenCalledWith(["PagamentoService"]);
        expect(roteador.post).toHaveBeenCalledWith("/validaCartaoDeCredito", expect.anything(), expect.anything(), expect.anything(), expect.anything(), expect.anything());
    });
});