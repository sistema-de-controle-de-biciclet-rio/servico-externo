const { FilaCobranca } = require("../../../src/filas/index.js");
const { Cobranca } = require("../mocks/entities.js");
const { RequestService, PagamentoService, TimeService } = require("../mocks/services.js");

describe("FilaCobranca", () => {
    let filaCobranca = {};

    beforeEach(() => {
        filaCobranca = new FilaCobranca({ 
            requestService: RequestService,
            pagamentoService: PagamentoService,
            timeService: TimeService
        });
    });

    describe("processar()", () => {
        test("Deve processar as cobranças com sucesso.", async () => {
            // Arrange
            const cobrancas = [
                Cobranca.new({ _id: 1 }),
                Cobranca.new({ _id: 2 }),
                Cobranca.new({ _id: 3 })
            ];

            cobrancas.forEach(cobranca => filaCobranca.add(cobranca));
            
            // Act
            await filaCobranca.processar();

            // Assert
            expect(cobrancas[0].cobrar).toHaveBeenCalledWith(RequestService, PagamentoService, TimeService);
            expect(cobrancas[1].cobrar).toHaveBeenCalledWith(RequestService, PagamentoService, TimeService);
            expect(cobrancas[2].cobrar).toHaveBeenCalledWith(RequestService, PagamentoService, TimeService);
            expect(filaCobranca._sucessos.length).toBe(3);
            expect(filaCobranca._falhas.length).toBe(0);
        });
        
        test("Deve adicionar a cobrança na coleção de falhas se houver um erro.", async () => {
            // Arrange
            let cobrancaFalha = Cobranca.new({ _id: 3 });

            cobrancaFalha.cobrar.mockImplementation(() => { throw Error("Falha ao realizar cobrança."); });

            const cobrancas = [
                Cobranca.new({ _id: 1 }),
                Cobranca.new({ _id: 2 }),
                cobrancaFalha,
            ];

            cobrancas.forEach(cobranca => filaCobranca.add(cobranca));

            // Act
            await filaCobranca.processar();

            // Assert
            expect(cobrancas[0].cobrar).toHaveBeenCalledWith(RequestService, PagamentoService, TimeService);
            expect(cobrancas[1].cobrar).toHaveBeenCalledWith(RequestService, PagamentoService, TimeService);
            expect(cobrancas[2].cobrar).toHaveBeenCalledWith(RequestService, PagamentoService, TimeService);
            expect(filaCobranca._sucessos.length).toBe(2);
            expect(filaCobranca._falhas.length).toBe(1);
            expect(filaCobranca._elementos.length).toBe(1);
        });
    });

    describe("remover()", () => {
        test("Deve remover uma cobrança da fila de processamento.", async () => {
            // Arrange
            const cobranca1 = Cobranca.new({ _id: 1 });
            const cobranca2 = Cobranca.new({ _id: 3 });

            const cobrancaParaRemover = Cobranca.new({ _id: 2 });
            const cobrancas = [
                cobranca1,
                cobrancaParaRemover,
                cobranca2
            ];

            cobrancas.forEach(cobranca => filaCobranca.add(cobranca));
            
            // Act
            filaCobranca.remover(cobrancaParaRemover);

            // Assert
            expect(filaCobranca._elementos).not.toContain(cobrancaParaRemover);
            expect(filaCobranca._elementos).toContain(cobranca1);
            expect(filaCobranca._elementos).toContain(cobranca2);
        });
    });

    describe("resultado()", () => {
        test("Deve retornar o resultado do processamento.", async () => {
            // Arrange
            const cobranca1 = Cobranca.new({ _id: 1 });
            const cobranca2 = Cobranca.new({ _id: 2 });
            const cobranca3 = Cobranca.new({ _id: 3 });
            
            filaCobranca._sucessos = [cobranca1, cobranca2];
            filaCobranca._falhas = [cobranca3];

            // Act
            const resultado = await filaCobranca.resultado();

            // Assert
            expect(resultado.sucessos).toContain(cobranca1);
            expect(resultado.sucessos).toContain(cobranca2);
            expect(resultado.falhas).toContain(cobranca3);
            expect(filaCobranca._sucessos.length).toBe(0);
            expect(filaCobranca._falhas.length).toBe(0);
        });
    });
});
