const { FilaEmail } = require("../../../src/filas/index.js");
const { Email } = require("../mocks/entities.js");
const { DotenvService, MailService } = require("../mocks/services.js");

describe("FilaEmail", () => {
    let filaEmail = {};

    beforeEach(() => {
        filaEmail = new FilaEmail({ mailService: MailService, dotenvService: DotenvService });
    });

    describe("processar()", () => {
        test("Deve processa a fila de email com sucesso.", async () => {
            // Arrange 
            const emails = [
                Email.new(),
                Email.new(),
                Email.new()
            ];
            
            emails.forEach(email => filaEmail.add(email));

            // Act
            await filaEmail.processar();

            // Assert
            expect(emails[0].enviar).toHaveBeenCalled();
            expect(emails[1].enviar).toHaveBeenCalled();
            expect(emails[2].enviar).toHaveBeenCalled();
            expect(filaEmail._sucessos.length).toBe(3);
            expect(filaEmail._falhas.length).toBe(0);
        });

        test("Deve adicionar um email na coleção de falhas se seu envio falhar.", async () => {
            // Arrange
            const emailFalho = Email.new();
            emailFalho.enviar.mockImplementation(() => {
                throw Error("Falha no envio do email.");
            });

            const emails = [
                Email.new(),
                emailFalho, 
                Email.new()
            ];
            
            emails.forEach(email => filaEmail.add(email));

            // Act
            await filaEmail.processar();

            // Assert
            expect(emails[0].enviar).toHaveBeenCalled();
            expect(emailFalho.enviar).toHaveBeenCalled();
            expect(emails[2].enviar).toHaveBeenCalled();
            expect(filaEmail._sucessos.length).toBe(2);
            expect(filaEmail._falhas.length).toBe(1);
        });
    });
});
