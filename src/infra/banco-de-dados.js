module.exports = class BancoDeDados {
    #tabelas;

    constructor(tabelas) {
        this.#tabelas = tabelas;
    }

    tabela = (nome) => {
        return this.#tabelas[nome];
    }

    clear = () => {
        for (const tabela in this.#tabelas) {
            this.#tabelas[tabela].clear();    
        }
    }
}
