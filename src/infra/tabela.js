module.exports = class Tabela {
    #linhas;
    #schema;
    #entidade;
    #indice;

    constructor(entidade, schema, linhas = {}, indice = 0) {
        this.#entidade = entidade; 
        this.#schema = schema;
        this.#linhas = linhas;
        this.#indice = indice;
    }
    
    clear = () => {
        this.#linhas = {};
        this.#indice = 0;
    }
    
    inserir = (dados) => {
        return new Promise(resolve => {
            this.#validarSchema(dados);

            const identificador = ++this.#indice;

            if (this.#schema?.id.geradoAutomaticamente) {
                dados["id"] = identificador;
            }
            
            this.#linhas[identificador] = dados;

            resolve((new this.#entidade(dados)));
        });
    }

    atualizar = (identificador, dados) => {
        return new Promise(resolve => {
            this.#validarSchema(dados, true);
            
            const registro = this.#linhas[identificador];

            if (registro.length === 0) {
                reject(new Error(`O registro ${identificador} não foi encontrado para ser atualizado.`));
            }

            this.#linhas[identificador] = {
                ...registro,
                ...dados
            };

            resolve();
        });
    }
    
    todos = (criterios = {}) => {
        return new Promise(resolve => {
            const filtro = this.#registrosFiltrados(criterios);

            let dados = {};

            if (Object.keys(criterios).length === 0) {
                dados = this.#linhas;
            } else {
                dados = filtro;
            }
            
            const colecao = [];

            for (const indice in dados) {
                const dado = dados[indice];
                colecao.push(new this.#entidade(dado));
            }

            resolve(colecao);
        });
    }

    remover = (id) => {
        return new Promise(resolve => {
            delete this.#linhas[id];

            resolve();
        });
    }

    salvar = (entidade) => {
        return new Promise(resolve => {
            const dadosCorrentes = this.#linhas[entidade.id];
            const novosDados = {};

            for (const chave in dadosCorrentes) {
                novosDados[chave] = entidade[chave]; 
            }
            
            delete novosDados["id"];

            this.#validarSchema(novosDados);

            novosDados["id"] = entidade.id;

            this.#linhas[entidade.id] = novosDados;
            
            resolve();
        });
    }

    unicoPorId = (id) => {
        return new Promise(resolve => {
            const dados = this.#linhas[id];

            if (dados === undefined) {
                return resolve([]);
            }

            resolve([new this.#entidade(dados)]);
        });
    }

    ultimoRegistroInserido = () => {
        return new Promise((resolve, reject) => {
            const registro = this.#linhas[this.#indice];

            if (registro === undefined) {
                reject(new Error("Não há registros no banco ou o último registro inserido foi removido.")); 
            }

            resolve([new this.#entidade(registro)]);
        });
    }

    #validarSchema = (dados, parcialmente = false) => {
        if (!parcialmente) {
            this.#verificarSeTodosOsDadosForamFornecidos(dados);
        }

        for (const chave in dados) {
            this.#validarChave(chave, dados);
        }
    }

    #verificarSeTodosOsDadosForamFornecidos = (dados) => {
        for (const coluna in this.#schema) {
            if (dados[coluna] === undefined && !this.#schema[coluna].geradoAutomaticamente) {
                throw Error(`Campo [${coluna}] não foi informado.`);
            }
        }
    }

    #validarChave = (chave, dados) => {
        const valorSchema = this.#schema[chave];
        
        if (valorSchema === undefined) {
            throw Error(`Campo inválido: ${chave}.`);
        }
        
        if (valorSchema.geradoAutomaticamente) {
            throw Error(`Campo [${chave}] é gerado automaticamente.`);
        }

        const dado = dados[chave];

        if (!(valorSchema.tipo.includes((typeof dado)))) {
            throw Error(`Tipo esperado para o campo [${chave}] é ${valorSchema.tipo}, mas foi fornecido o tipo ${typeof dado}.`);
        }

        if (!valorSchema.permiteNulo && !valorSchema?.geradoAutomaticamente && (dado === null || dado === undefined)) {
            throw Error(`O campo ${chave} não permite valores nulos.`);
        }
    }

    #registrosFiltrados = (criterios) => {
        const filtro = {};
        
        for (const indice in this.#linhas) {
            for (const coluna in criterios) {
                if (this.#linhas[indice][coluna] && this.#linhas[indice][coluna] === criterios[coluna]) {
                    filtro[indice] = this.#linhas[indice]; 
                }
            }
        }

        return filtro;
    }

}
