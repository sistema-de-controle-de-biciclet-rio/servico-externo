const BancoDeDados = require("./banco-de-dados.js");
const Tabela = require("./tabela.js");

module.exports = {
    BancoDeDados,
    Tabela
};
