const container = require("./dependencias.js");
const { FilaCobranca } = require("./filas/index.js");

const [ request, pagamento, time ] = container.get([ "RequestService", "PagamentoService", "TimeService" ]);
module.exports = new FilaCobranca({ requestService: request, pagamentoService: pagamento, timeService: time });