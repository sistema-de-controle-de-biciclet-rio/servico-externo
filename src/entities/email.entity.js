module.exports = class Email {
    #email;
    #assunto;
    #mensagem;

    constructor({ id, email, assunto, mensagem }) {    
        this._id = id; 
        this.#email = email;
        this.#assunto = assunto;
        this.#mensagem = mensagem;
    }

    get id() {
        return this._id;
    }

    get email() {
        return this.#email;
    }
    
    enviar = async (remetente, mailService) => {
        await mailService.enviar({ from: remetente, to: this.#email, subject: this.#assunto, message: this.#mensagem });
    }
    
    valido = () => {
        return (/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.exec(this.#email)) !== null;
    }

    convertidoParaObjeto = () => {
        return {
            id: this._id,
            email: this.#email,
            assunto: this.#assunto,
            mensagem: this.#mensagem
        }
    }

    igual = (outro) => {
        return this._id === outro._id;
    }
}
