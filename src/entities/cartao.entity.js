module.exports = class Cartao {
    #nomeTitular;
    #numero;
    #validade;
    #cvv;

    constructor({ id = 0, nomeTitular, numero, validade, cvv }) {
        this._id = id;
        this.#nomeTitular = nomeTitular;
        this.#numero = numero;
        this.#validade = validade;
        this.#cvv = cvv;
    }

    validar = async (pagamentoService) => {
        const numeroInvalido = /^\d{16}$/.exec(this.#numero) === null;
        const cvvInvalido = /^\d{3}$/.exec(this.#cvv) === null;
        const validadeInvalida = /^\d{4}-\d{2}-\d{2}$/.exec(this.#validade) === null;

        if (numeroInvalido) {
            throw Error("Número do cartão inválido: deve estar no formato 9999999999999999.");
        }

        if (cvvInvalido) {
            throw Error("CVV inválido: deve estar no formato 999.");
        }

        if (validadeInvalida) {
            throw Error("Validade inválida: deve estar no formato 9999-99-99");
        }

        await pagamentoService.validarCartao({ nomeTitular: this.#nomeTitular, numero: this.#numero, validade: this.#validade, cvv: this.#cvv });
    }
}
