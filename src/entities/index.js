const Email = require("./email.entity.js");
const Cobranca = require("./cobranca.entity.js");
const Cartao = require("./cartao.entity.js");

module.exports = { 
    Email,
    Cobranca,
    Cartao
}
