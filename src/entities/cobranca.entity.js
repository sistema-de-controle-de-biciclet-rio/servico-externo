const { StatusCobranca } = require("../enums.js");
const { DotenvService } = require("../services/index.js");

module.exports = class Cobranca {
    #valor;
    #status;
    #horaSolicitacao;
    #horaFinalizacao;
    #ciclista;

    constructor(data) {
        const { id, status, horaSolicitacao, horaFinalizacao, valor, ciclista } = data;

        this._id = id; 
        this.#valor = valor;
        this.#status = status;
        this.#horaSolicitacao = horaSolicitacao;
        this.#horaFinalizacao = horaFinalizacao;
        this.#ciclista = ciclista;
    }
    
    cobrar = async (requestService, pagamentoService, timeService) => {

        const dotenv = new DotenvService();
        const aluguelUrl = dotenv.get("ALUGUEL_URL");

        let resultado = await requestService.get(`${aluguelUrl}/cartaoDeCredito/${this.#ciclista}`);
        console.log(resultado); 
        if (!resultado?.success) {
            this.#status = StatusCobranca.FALHA;
            throw Error(`Erro ao recuperar cartao de crédito do ciclista ${this.#ciclista}.`); 
        }

        const { nomeTitular = "teste", numero = "5398.1452.0487.8634", validade = "2027-01-27", cvv = "124"} = resultado.data[0];
        
        const result = await pagamentoService.criarTransacao({
            cvv,
            numero,
            titular: nomeTitular,
            validade,
            valor: this.#valor
        });

        console.log(result);
        
        if (!result.success) {
            this.#status = StatusCobranca.FALHA;
            throw Error(result.message);
        }

        this.#horaFinalizacao = timeService.horaAtual();
        this.#status = StatusCobranca.PAGA;
    }

    igual = (outro) => {
        return this._id === outro._id; 
    }

    get id() {
        return this._id;
    }

    get valor() {
        return this.#valor;
    }

    get horaSolicitacao() {
        return this.#horaSolicitacao;
    }

    get horaFinalizacao() {
        return this.#horaFinalizacao;
    }

    get status() {
        return this.#status;
    }

    get ciclista() {
        return this.#ciclista;
    }


    convertidoParaObjeto = () => {
        return {
            id: this._id,
            valor: this.#valor,
            status: this.#status,
            horaSolicitacao: this.#horaSolicitacao,
            horaFinalizacao: this.#horaFinalizacao,
            ciclista: this.#ciclista
        };
    }

    convertidoParaHTML = () => {
        return `<div>
                <p>Valor cobrado: R$ ${Number.parseFloat(this.#valor).toFixed(2)}</p>
                <p>Status da cobrança: ${this.#status}</p>
                <p>Hora da Solicitação: ${(new Date(this.#horaSolicitacao)).toLocaleString("pt-BR")}</p>
                <p>Hora da Finalização: ${(new Date(this.#horaFinalizacao)).toLocaleString("pt-BR")}</p>
            <div>`
    }
}
