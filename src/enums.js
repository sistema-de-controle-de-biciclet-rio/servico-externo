module.exports = {
    StatusCobranca: {
        PENDENTE: "PENDENTE",
        PAGA: "PAGA",
        FALHA: "FALHA",
        CANCELADA: "CANCELADA",
        OCUPADA: "OCUPADA"
    },
    TipoDeDado: {
        STRING: "string",
        OBJECT: "object",
        NUMBER: "number"
    }
}
