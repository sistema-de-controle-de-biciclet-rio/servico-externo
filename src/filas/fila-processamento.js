module.exports = class FilaProcessamento {
    constructor({ elementos = [] }) {
        this._elementos = elementos;
        this._sucessos = [];
        this._falhas = [];
    }

    add = (elemento) => {
        this._elementos.push(elemento);
    }

    remover = (elemento) => {
        this._elementos = this._elementos.filter(elementoNaFila => {
            return !elementoNaFila.igual(elemento);
        });
    }

    resultado = () => {
        return new Promise(resolve => {
            const sucessos = this._sucessos;
            const falhas = this._falhas;
            
            this._sucessos = [];
            this._falhas = [];
            resolve({ sucessos, falhas });
        });
    }
    
    vazia = () => {
        return this._elementos.length === 0;
    }

    limpar = () => {
        this._elementos = [];
        this._sucessos = [];
        this._falhas = [];
    }
}
