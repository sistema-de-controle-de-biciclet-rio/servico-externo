const FilaProcessamento = require("./fila-processamento.js");
module.exports = class FilaCobranca extends FilaProcessamento {
    #axiosService
    #pagamentoService
    #timeService;

    constructor({ elementos = [], requestService, pagamentoService, timeService }) {
        super({ elementos });

        this.#axiosService = requestService;
        this.#pagamentoService = pagamentoService;
        this.#timeService = timeService;
    }


    /**
     * @public
     * @returns {{sucessos: Array, erros: Array}}
     */
    processar = async () => {
        const processamentos = [];

        for (const cobranca of this._elementos) {
            processamentos.push(this.#processarCobranca(cobranca));
        }
        
        await Promise.allSettled(processamentos);
    }

    #processarCobranca = async (cobranca) => {
        try {
            await cobranca.cobrar(this.#axiosService, this.#pagamentoService, this.#timeService);

            this.remover(cobranca);
            this._sucessos.push(cobranca);
        } catch (err) {
            console.log(err);
            this._falhas.push(cobranca); 
        }
    }
}


