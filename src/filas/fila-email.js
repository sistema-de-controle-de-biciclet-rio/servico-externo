const FilaProcessamento = require("./fila-processamento.js");
    
module.exports = class FilaEmail extends FilaProcessamento {
    #mailService;
    #dotenvService;

    constructor({ elementos = [], mailService, dotenvService }) {
        super({ elementos });

        this.#dotenvService = dotenvService;
        this.#mailService = mailService;
    }

    processar = async () => {
        this._elementos.forEach(email => {
            this.#processarEmail(email); 
        });
    }

    #processarEmail = async (email) => {
        try {
            await email.enviar(this.#dotenvService.get("MAIL_SENDER"), this.#mailService);
            this.remover(email); 
            this._sucessos.push(email);
        } catch (err) {
            this._falhas.push(email);
        }
    }
}
