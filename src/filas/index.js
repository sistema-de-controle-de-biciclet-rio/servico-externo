const FilaEmail = require("./fila-email.js");
const FilaCobranca = require("./fila-cobranca.js");

module.exports = {
    FilaEmail,
    FilaCobranca
};
