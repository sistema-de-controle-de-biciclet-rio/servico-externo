const container = require("./dependencias.js");
const App = require("./app.js");
const server = require("./server.js");
const populate = require("./populate.js");

const app = new App({
    servidorWeb: server,
    containerDeDependencias: container
});

populate().then(() => {
    app.iniciar();
});
