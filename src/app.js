module.exports = class App {
    #servidorWeb;
    #containerDeDependencias;
    
    constructor({ servidorWeb, containerDeDependencias }) {
        this.#servidorWeb = servidorWeb;
        this.#containerDeDependencias = containerDeDependencias;
    }

    iniciar = () => {
        const [dotenv, axiosService] = this.#containerDeDependencias.get(["DotenvService", "RequestService"]);

        
        const port = dotenv.get("APP_PORT");
        const host = dotenv.get("APP_HOST");
        const dozeHoras = 1000*60*60*12;

        this.#servidorWeb.listen(port, () => {
            console.log(`Listening on port ${port}`);
    
            setInterval(() => {
                axiosService.post(`${host}:${port}/processaCobrancasEmFila`)
                    .then(res => {
                        console.log(res.data);
                    })
                    .catch(err => {
                        console.log(err.message);
                    });
        
            }, dozeHoras);
        });
    }
};
