const EmailController = require("./email.controller.js");
const CobrancaController = require("./cobranca.controller.js");
const CartaoController = require("./cartao.controller.js");

module.exports = {
    EmailController,
    CobrancaController,
    CartaoController
}
