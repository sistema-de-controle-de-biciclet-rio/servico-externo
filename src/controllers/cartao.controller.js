module.exports = class CartaoController {
    #pagamentoService;
    #datasource;
    #validationResult;

    constructor({ datasource, pagamentoService, validationResult }) {
        this.#pagamentoService = pagamentoService;
        this.#datasource = datasource;
        this.#validationResult = validationResult;
    }

    validarCartaoDeCredito = async (request, response, next) => {
        const resultado = this.#validationResult(request);

        if (!resultado.isEmpty()) {
            return next({ codigo: 422, mensagem: resultado.array().map(objeto => objeto.msg) }) 
        }

        try {
            const tabela = this.#datasource.tabela("cartao");
            await tabela.inserir(request.body);
            const [cartao] = (await tabela.ultimoRegistroInserido());
            
            await cartao.validar(this.#pagamentoService);
            
            response.status(200).end(); 
        } catch (err) {
            next([{ codigo: 422, mensagem: err.message }]);
        }

    };
}
