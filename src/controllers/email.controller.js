const { validationResult } = require("express-validator");

module.exports = class EmailController {
    #mailService;
    #dotenvService;
    #datasource;
    #validationResult;

    constructor({ mailService, dotenvService, datasource, validationResult }) {
        this.#mailService = mailService;
        this.#dotenvService = dotenvService;
        this.#datasource = datasource;
        this.#validationResult = validationResult;
    }

    enviarEmail = async (request, response, next) => {
        const resultado = this.#validationResult(request);

        if (!resultado.isEmpty()) {
            return next([{ codigo: 422, mensagem: resultado.array().map(objeto => objeto.msg) }]); 
        }

        try {
            const { email: destinatario, assunto, mensagem } = request.body;
            
            const tabelaEmail = this.#datasource.tabela("email");

            await tabelaEmail.inserir({ email: destinatario, assunto, mensagem });
            const [email] = (await tabelaEmail.ultimoRegistroInserido());
            
            if (!email.valido()) {
                return next([{ mensagem: "E-mail com formato inválido.", codigo: 422 }]);
            }
            
            await email.enviar(this.#dotenvService.get("MAIL_SENDER"), this.#mailService);

            return response.status(200).json(email.convertidoParaObjeto());
        } catch (err) {
            return next({ codigo: 404, mensagem: err.message });
        }

    }
}
