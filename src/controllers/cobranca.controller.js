const { StatusCobranca } = require("../enums.js");
const { validationResult } = require("express-validator");

module.exports = class CobrancaController {
    #filaEmail;
    #datasource;
    #filaCobranca;
    #pagamentoService;
    #requestService;
    #timeService;
    #dotenvService;
    #validationResult;

    constructor({ filaEmail, filaCobranca, pagamentoService, requestService, datasource, timeService, dotenvService, validationResult }) {
        this.#filaCobranca = filaCobranca;
        this.#filaEmail = filaEmail;
        this.#datasource = datasource;
        this.#pagamentoService = pagamentoService;
        this.#requestService = requestService;
        this.#timeService = timeService;
        this.#dotenvService = dotenvService;
        this.#validationResult = validationResult;
    }

    realizarCobranca = async (request, response, next) => {
        const resultado = this.#validationResult(request);

        if (!resultado.isEmpty()) {
            return next({ codigo: 422, mensagem: resultado.array().map(objeto => objeto.msg) }) 
        }

        let cobranca;
        const tabelaCobranca = this.#datasource.tabela("cobranca");
        try {
            const horaSolicitacao = this.#timeService.horaAtual();

            const { valor, ciclista } = request.body;
            
            await tabelaCobranca.inserir({ valor, ciclista, status: StatusCobranca.PENDENTE, horaSolicitacao, horaFinalizacao: "" });
            
            [cobranca] = await tabelaCobranca.ultimoRegistroInserido();
            
            await cobranca.cobrar(this.#requestService, this.#pagamentoService, this.#timeService);
            
            await tabelaCobranca.salvar(cobranca);

            return response.status(200).json(cobranca.convertidoParaObjeto()); 
        } catch (err) {
            if (cobranca !== undefined) {
                await tabelaCobranca.salvar(cobranca);
            }

            return next([{ codigo: 422, mensagem: err.message }]);
        }
    }

    incluirCobrancaEmFilaCobranca = async (request, response, next) => {
        const resultado = this.#validationResult(request);
        
        if (!resultado.isEmpty()) {
            return next({ codigo: 422, mensagem: resultado.array().map(objeto => objeto.msg) }) 
        }

        try { 
            const horaSolicitacao = this.#timeService.horaAtual();
            const { valor, ciclista } = request.body;
            const tabelaCobranca = this.#datasource.tabela("cobranca");

            await tabelaCobranca.inserir({ valor, ciclista, status: StatusCobranca.FALHA, horaSolicitacao, horaFinalizacao: "" }); 
            const [cobranca] = (await tabelaCobranca.ultimoRegistroInserido());      

            this.#filaCobranca.add(cobranca);
        
            return response.status(200).json(cobranca.convertidoParaObjeto());
        } catch (err) {
            return next([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    obterCobranca = async (request, response, next) => {
        const resultado = this.#validationResult(request);
        
        if (!resultado.isEmpty()) {
            return next({ codigo: 422, mensagem: resultado.array().map(objeto => objeto.msg) }) 
        }

        const { idCobranca } = request.params;
        
        const tabelaCobranca = this.#datasource.tabela("cobranca");

        const [cobranca] = await tabelaCobranca.unicoPorId(idCobranca);

        if (cobranca === undefined) {
            return next({
                codigo: 404,
                mensagem: "Cobrança não encontrada."
            });
        }

        return response.status(200).json(cobranca.convertidoParaObjeto());
    }

    processarFilaCobranca = async (request, response, next) => {
        console.log(this.#filaCobranca);
        await this.#filaCobranca.processar();
        
        const { sucessos, falhas } = await this.#filaCobranca.resultado();

        console.log([sucessos, falhas]);
        
        await this.#enviarEmails(sucessos);
       
        const cobrancasAtualizadas = [];

        for (const cobranca of sucessos) {
            cobrancasAtualizadas.push(this.#datasource.tabela("cobranca").salvar(cobranca));
        }

        await Promise.all(cobrancasAtualizadas);

        console.log(cobrancasAtualizadas);
        
        if (falhas.length > 0) {
            return next(falhas.map(falha => {
                return { codigo: 422, mensagem: `Falha ao processar cobrança de id ${falha.id}.` };
            }));
        }

        response.status(200).json(sucessos.map(cobranca => {
            return cobranca.convertidoParaObjeto()
        }).sort((a, b) => a.id < b.id ? -1 : 1));
    }

    
    #enviarEmails = async (sucessos) => {
        const tabelaEmail = this.#datasource.tabela("email");

        for (const cobranca of sucessos) {     
            await tabelaEmail.inserir({ 
                    email: this.#dotenvService.get("MAIL_RECEIVER"), 
                    mensagem: cobranca.convertidoParaHTML(), 
                    assunto: "Cobrança em atraso" 
            });
        
            this.#filaEmail.add(await tabelaEmail.ultimoRegistroInserido());
        };
        
        await this.#filaEmail.processar();
    }
}
