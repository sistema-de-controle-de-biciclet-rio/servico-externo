const { CobrancaController } = require("../controllers/index.js");
const { body, param, validationResult } = require("express-validator");
module.exports = class RotasDeCobranca {
    #container;
    #datasource;
    #filaCobranca;

    constructor(container, datasource, filaCobranca) {
        this.#container = container;
        this.#datasource = datasource;;
        this.#filaCobranca = filaCobranca;
    }

    registrar = (roteador) => {
        const [
            filaEmail,
            requestService,
            timeService,
            pagamentoService,
            dotenvService
        ] = this.#container.get(["FilaEmail", "RequestService", "TimeService", "PagamentoService", "DotenvService"]);

        const controller = new CobrancaController({ 
            filaEmail, 
            filaCobranca: this.#filaCobranca, 
            requestService, 
            timeService, 
            datasource: this.#datasource, 
            pagamentoService,
            dotenvService,
            validationResult: validationResult
        });

        roteador.get("/cobranca/:idCobranca", 
            param("idCobranca")
                .trim()
                .isInt({ min: 0 }).withMessage("O id da cobrança deve ser um inteiro igual ou maior do que 0.")
                .notEmpty().withMessage("O id da cobrança deve ser informado."), 
            controller.obterCobranca
        );

        roteador.post("/filaCobranca",
            body("valor")
                .isFloat({ min: 0 })
                .notEmpty(),
            body("ciclista")
                .isInt({ min: 0 })
                .notEmpty(),
            controller.incluirCobrancaEmFilaCobranca
        );

        roteador.post("/cobranca", 
            body("valor")
                .notEmpty().withMessage("O valor da cobrança deve ser informado.")
                .isFloat({ min: 0 }).withMessage("O valor da cobrança deve ser igual ou maior do que 0."),
            body("ciclista")
                .notEmpty().withMessage("O id do ciclista deve ser informado.")
                .isInt({ min: 0 }).withMessage("O id do ciclista deve ser um inteiro igual ou maior do que 0."),
            controller.realizarCobranca
        );

        roteador.post("/processaCobrancasEmFila", controller.processarFilaCobranca);

        return roteador;
    }
}
