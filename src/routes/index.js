const RotasDeEmail = require("./email.routes.js");
const RotasDeCobranca = require("./cobranca.routes.js");
const RotasDeCartao = require("./cartao.routes.js");

module.exports = { 
    RotasDeEmail,
    RotasDeCobranca,
    RotasDeCartao 
}
