const { EmailController } = require("../controllers/index.js");
const { body, validationResult } = require("express-validator");
module.exports = class RotasDeEmail {
    #container;
    #datasource;

    constructor(container, datasource) {
        this.#container = container;
        this.#datasource = datasource;
    }

    registrar = (roteador) => {
        const [mailService, dotenvService] = this.#container.get(["MailService", "DotenvService"]);

        const controller = new EmailController({ mailService, dotenvService, datasource: this.#datasource, validationResult: validationResult });
        
        roteador.post("/enviarEmail",
            body("assunto")
                .trim()
                .notEmpty().withMessage("O assunto do email deve ser informado.")
                .isString().withMessage("O assunto do email deve ser um texto."),
            body("mensagem")
                .trim()
                .notEmpty().withMessage("A mensagem do email deve ser informada.")
                .isString().withMessage("A mensagem do email deve ser um texto."),
            body("email")
                .trim()
                .notEmpty().withMessage("O email do email deve ser informado.")
                .isString().withMessage("O email do email deve ser um texto."),
            controller.enviarEmail
        );

        return roteador;
    }
}
