const { CartaoController } = require("../controllers/index.js");
const { body, validationResult } = require("express-validator");
module.exports = class RotasDeCartao {
    #container;
    #datasource;

    constructor(container, datasource) {
        this.#container = container;
        this.#datasource = datasource;
    }

    registrar = (roteador) => {
        const [pagamentoService] = this.#container.get(["PagamentoService"]);

        const controller = new CartaoController({ datasource: this.#datasource, pagamentoService, validationResult });

        roteador.post("/validaCartaoDeCredito",
            body("nomeTitular")
                .trim()
                .notEmpty().withMessage("O nome do titular deve ser informado.")
                .isString(),
            body("numero")
                .trim()
                .notEmpty().withMessage("O numero do cartão deve ser informado.")
                .isString(),
            body("validade")
                .trim()
                .notEmpty().withMessage("A validade do cartão deve ser informado.")
                .isString(),
            body("cvv")
                .trim()
                .notEmpty().withMessage("O cvv do cartão deve ser informado.")
                .isString(),
            controller.validarCartaoDeCredito
        );

        return roteador;
    }
}
