const { Email, Cobranca, Cartao } = require("./entities/index.js");
const { BancoDeDados, Tabela } = require("./infra/index.js");
const { TipoDeDado } = require("./enums.js");

const email = new Tabela(Email, {
    id: {
        tipo: TipoDeDado.NUMBER,
        geradoAutomaticamente: true,
    },
    assunto: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false 
    },
    email: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false 
    },
    mensagem: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false
    }
});

const cobranca = new Tabela(Cobranca, {
    id: {
        tipo: TipoDeDado.NUMBER,
        geradoAutomaticamente: true
    },
    valor : {
        tipo: TipoDeDado.NUMBER,
        permiteNulo: false
    },
    status: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false
    },
    horaSolicitacao: {
        tipo: `${TipoDeDado.OBJECT}|${TipoDeDado.STRING}`,
        permiteNulo: false
    },
    horaFinalizacao: {
        tipo: `${TipoDeDado.OBJECT}|${TipoDeDado.STRING}`,
        permiteNulo: false
    },
    ciclista: {
        tipo: TipoDeDado.NUMBER,
        permiteNulo: false
    }
});

const cartao = new Tabela(Cartao, {
    id: {
        tipo: TipoDeDado.NUMBER,
        geradoAutomaticamente: true
    },
    nomeTitular: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false
    },
    numero: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false
    },
    validade: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false
    },
    cvv: {
        tipo: TipoDeDado.STRING,
        permiteNulo: false
    }
});

const banco = new BancoDeDados({ email, cobranca, cartao });

console.log("Banco inicializado.");

module.exports = banco;
