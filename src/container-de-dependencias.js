module.exports = class ContainerDeDependencias {
    #dependencias;

    constructor(dependencias) {
        this.#dependencias = dependencias;
    }

    get = (chaves) => {
        return chaves.map(chave => {
            const dependencia = this.#dependencias[chave];

            if (dependencia === undefined) {
                throw Error("Dependência não registrada: " + chave);
            }

            return dependencia(this);
        });
    }
}
