const fs = require("node:fs/promises");
const FilaCobranca = require("./fila.js");
const Datasource = require("./datasource.js");
const moment = require("moment");

module.exports = async () => {
    const amostra = await fs.readFile("./amostra-externo.json", { encoding: "utf-8" })
    const { cobrancas } = JSON.parse(amostra);
    
    const tabela = Datasource.tabela("cobranca");
    const insercoes = [];

    cobrancas.forEach(cobranca => {
        insercoes.push(tabela.inserir({ ...cobranca, horaSolicitacao: moment(), horaFinalizacao: "" }));
    });

    Promise.all(insercoes).then(resultados => {
        resultados.forEach(resultado => {
            FilaCobranca.add(resultado)
        });
    });
}