const express = require("express");
const { RotasDeCobranca, RotasDeCartao, RotasDeEmail } = require("./routes/index.js");
const Datasource = require("./datasource.js");
const Container = require("./dependencias.js");
const filaCobranca = require("./fila.js");
const populate = require("./populate.js");

const server = express();
server.use(express.json());
server.disable("x-powered-by");

const roteador = express.Router();

server.use((new RotasDeCobranca(Container, Datasource, filaCobranca)).registrar(roteador));
server.use((new RotasDeCartao(Container, Datasource)).registrar(roteador));
server.use((new RotasDeEmail(Container, Datasource)).registrar(roteador));
server.get("/restaurarDados", async (req, res) => {
    try {
        Datasource.clear();
        filaCobranca.limpar();
        console.log(filaCobranca);
        await populate();
    } catch (err) {
        return res.status(500).json({ mensagem: err.message });
    }

    return res.status(200).end();
});

server.use((error, request, response, next) => {
    let statusCode = Array.isArray(error) ? error[0].codigo : error.codigo;

    if (statusCode === undefined) {
        statusCode = 500;
    }

    return response.status(statusCode).json(error);
});

module.exports = server;