const ContainerDeDependencias = require("./container-de-dependencias.js");
const { default: axios } = require("axios");
const { 
    DotenvService,
    MomentService,
    AxiosService,
    MailjetService,
    CieloService
} = require("./services/index.js");
const { FilaEmail, FilaCobranca } = require("./filas/index.js");

const container = new ContainerDeDependencias({
    "DotenvService": () => {
        return new DotenvService();
    },
    "TimeService": () => {
        return new MomentService();
    },
    "RequestService": () => {
        return new AxiosService(axios); 
    },
    "RequestCielo": (isso) => {
        const [ dotenv ] = isso.get([ "DotenvService" ]);
        return new AxiosService(axios, { 
            baseUrl: dotenv.get("CIELO_HOST"), 
            options: {
                headers: {
                    "Content-Type": "application/json",
                    MerchantId: dotenv.get("CIELO_MERCHANT_ID"),
                    MerchantKey: dotenv.get("CIELO_MERCHANT_KEY")
                }
            }
        });
    },
    "MailService": (isso) => {
        const [ dotenv ] = isso.get([ "DotenvService" ]);
        return new MailjetService({ publicKey: dotenv.get("MAILJET_PUBLIC_KEY"), privateKey: dotenv.get("MAILJET_PRIVATE_KEY") });
    },
    "PagamentoService": (isso) => {
        const [ request, time ] = isso.get([ "RequestCielo", "TimeService" ]);
        return new CieloService({ timeService: time, requestService: request });
    },
    "FilaCobranca": (isso) => {
        const [ request, pagamento, time ] = isso.get([ "RequestService", "PagamentoService", "TimeService" ]);
        return new FilaCobranca({ requestService: request, pagamentoService: pagamento, timeService: time });
    },
    "FilaEmail": (isso) => {
        const [ dotenv, mail ] = isso.get([ "DotenvService", "MailService" ]);
        return new FilaEmail({ dotenvService: dotenv, mailService: mail });
    }
});

module.exports = container;
