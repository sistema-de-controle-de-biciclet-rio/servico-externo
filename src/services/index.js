const DotenvService = require("./env/dotenv.service.js");
const MailjetService = require("./email/mailjet.service.js");
const AxiosService = require("./request/axios.service.js");
const CieloService = require("./pagamento/cielo.service.js");
const MomentService = require("./time/moment.service.js");

module.exports = {
    DotenvService,
    MailjetService,
    AxiosService,
    CieloService,
    MomentService
}