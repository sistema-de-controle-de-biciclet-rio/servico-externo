module.exports = class CieloService {
    #requestService;
    #timeService;

    /**
     * 
     * @param {{requestService: AxiosService, timeService: MomentService}} dependencias 
     */
    constructor(dependencias) {
        const { requestService, timeService } = dependencias;

        this.#requestService = requestService;
        this.#timeService = timeService;
    }

    /**
     * 
     * @param {{ numero: string, validade: string, cvv: string, titular: string }} dados 
     */
    criarTransacao = async (dados) => {

        const { numero, validade, cvv, titular, valor } = dados;

        const response = await this.#requestService.post("/1/sales/", {
            MerchantOrderId: "1",
            Customer: {
                Name: titular
            },
            Payment: {
                Amount: valor,
                CreditCard: {
                    Brand: "Visa",
                    CardNumber: numero,
                    SecurityCode: cvv,
                    Holder: "teste",
                    ExpirationDate: this.#timeService.formatar(validade, "MM/YYYY")
                },
                Installments: 1,
                Type: "CreditCard"
            }
        });
        
        if (!response.success) {
            return { code: -1, success: false, message: response.message };
        }

        const { ReturnCode, ReturnMessage } = response.data.Payment;

        let success = ReturnCode === "4" || ReturnCode === "6";

        return { code: ReturnCode, message: ReturnMessage, success: success };
    }

    validarCartao = async ({ nomeTitular, numero, cvv, validade }) => {
        const response = await this.#requestService.post("/1/zeroauth", {
            CardNumber: numero,
            Holder: nomeTitular,
            ExpirationDate: this.#timeService.formatar(validade, "MM/YYYY"),
            SecurityCode: cvv,
            SaveCard: false,
            Brand: "Visa",
            CardOnFile: {
                Usage: "First",
                Reason: "Recurring"
            }
        });
        
        if (!response.success) {
            throw Error("Houve um problema ao tentar validar o cartão. Tente mais tarde.");
        };
        
        if (!response.data?.Valid) {
            throw Error(response.data.ReturnMessage);
        }
    }
}
