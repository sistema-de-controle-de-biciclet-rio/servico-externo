const Mailjet = require("node-mailjet");

module.exports = class MailjetService {
    #service;

    constructor({ publicKey, privateKey }) {
        this.#service = new Mailjet({
            apiKey: publicKey,
            apiSecret: privateKey 
        });
    }

    /**
     * @param {{to: string, from: string, subject: string, message: string}} payload 
     */
    enviar = async ({ to, from, subject, message }) => {
        await this.#service.post("send", { version: "v3.1" }).request({
            Messages: [
                {
                    From: {
                        Email: from,
                        Name: "Sistema de Bicicletário"
                    },
                    To: [
                        {
                            Email: to,
                            Name: "You"
                        }
                    ],
                    Subject: subject,
                    HTMLPart: `<p>${message}</p>`
                }
            ]
        });
    }

    /**
     * 
     * @param {string} email 
     * @returns bool
     */
}
