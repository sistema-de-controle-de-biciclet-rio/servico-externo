const moment = require("moment");

module.exports = class MomentService {
    constructor() {
        this.service = moment;
    }

    horaAtual = () => {
        return this.service();
    }

    formatar = (date, format) => {
        return this.service(date).format(format);
    }
}