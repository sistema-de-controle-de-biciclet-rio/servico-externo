require("dotenv").config();

module.exports = class DotenvService {
    get = (key) => {
        return process.env[key];
    }
}